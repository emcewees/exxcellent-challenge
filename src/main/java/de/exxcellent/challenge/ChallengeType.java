package de.exxcellent.challenge;

/**
 * ChallengeType to hold challenge specific values.
 * 
 * TODO: Inherit from base class.
 */
public class ChallengeType {
    String name;
    int fieldOfInterest1;
    int fieldOfInterest2;


    public ChallengeType(String name, int fieldOfInterest1, int fieldOfInterest2) {
        this.name = name;
        this.fieldOfInterest1 = fieldOfInterest1;
        this.fieldOfInterest2 = fieldOfInterest2;


    }
} 