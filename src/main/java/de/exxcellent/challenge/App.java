package de.exxcellent.challenge;

import org.apache.commons.cli.*;

import de.exxcellent.reader.CSVReader;
import de.exxcellent.computation.Computation;
import de.exxcellent.exceptions.NoDataException;

import java.net.URL;
import java.io.File;
import java.util.ArrayList;



/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {

    static String challengeResourcePath = "./de/exxcellent/challenge/";

    /**
     * 
     */
    static ChallengeType challenge;
    static int fieldOfInterestTemp1;
    static int fieldOfInterestTemp2;
    

    /**
     * This is the main entry method of your program.
     * @param args The CLI arguments passed
     */
    public static void main(String... args) {

        // Adding available options 
        Options options = new Options();

        Option weather = new Option("w", "weather", true, "Challenge Type Weather");
        options.addOption(weather);

        Option football = new Option("f", "football", true, "Challenge Type Football");
        options.addOption(football);

        Option firstField = new Option("n1", "fieldNameNr1", true, "Field Nr 1");
        options.addOption(firstField);

        Option secondField = new Option("n2", "fieldNameNr2", true, "Field Nr 2");
        options.addOption(secondField);


        // Start parsing
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        String fileName = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("w"))
            {
                fileName = cmd.getOptionValue('w');
                if(cmd.hasOption("n1") && cmd.hasOption("n2")) {
                    fieldOfInterestTemp1 = Integer.parseInt(cmd.getOptionValue("n1"));	
                    fieldOfInterestTemp2 = Integer.parseInt(cmd.getOptionValue("n2"));
                } else {
                    fieldOfInterestTemp1 = 1;
                    fieldOfInterestTemp2 = 2;
                }
                challenge = new ChallengeType("weather", fieldOfInterestTemp1, fieldOfInterestTemp2);

                System.out.printf("Using Challenge: %s with fields: %d, %d\n", challenge.name, challenge.fieldOfInterest1, challenge.fieldOfInterest2 );

                
            }
            else if (cmd.hasOption("f"))
            {
                fileName = cmd.getOptionValue('f');
                if(cmd.hasOption("n1") && cmd.hasOption("n2")) {
                    fieldOfInterestTemp1 = Integer.parseInt(cmd.getOptionValue("n1"));	
                    fieldOfInterestTemp2 = Integer.parseInt(cmd.getOptionValue("n2"));
                } else {
                    fieldOfInterestTemp1 = 5;
                    fieldOfInterestTemp2 = 6;
                }

                challenge = new ChallengeType("football", fieldOfInterestTemp1, fieldOfInterestTemp2);
                System.out.printf("Using Challenge: %s with fields: %d, %d\n", challenge.name, challenge.fieldOfInterest1, challenge.fieldOfInterest2 );

            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("mvn exec:java", options);
            //System.exit(1);
        } 

        if(fileName != null) {
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            String fullPath = challengeResourcePath.concat(fileName);


            try {
                URL url = classLoader.getResource(fullPath);
                File file = new File(url.getFile());
                file.getAbsolutePath();
    
                CSVReader reader = new CSVReader();
                reader.readFile(file.getAbsolutePath());
                ArrayList<String[]> result = CSVReader.getData();
                Computation comp = new Computation();

                if (result.size() == 0) {
                    NoDataException e = new NoDataException("No data in file");
                    throw e;
                }

                // Range Check of data and selected fields. Assumes equally long rows in data.
                if (result.get(0).length < fieldOfInterestTemp1 || result.get(0).length < fieldOfInterestTemp2) {
                    IndexOutOfBoundsException e = new IndexOutOfBoundsException();
                    throw e;
                }
   
                else {
                    //CSVReader.printData();
                }
    
                // Different output depending on Challenge Type.
                if (challenge.name == "weather") {
                    System.out.printf(">> Day with smallest temperature spread:\n>> Day %s°C Temperature spread\n", comp.calculateMinAbsDiff(result, challenge.fieldOfInterest1, challenge.fieldOfInterest2)); 
                }
    
                if (challenge.name == "football") {
                    System.out.printf(">> Team with smallest goal spread:\n>> Team %s Goals absolute Difference\n", comp.calculateMinAbsDiff(result, challenge.fieldOfInterest1, challenge.fieldOfInterest2)); 
                }
                
            } catch (NullPointerException e) {
                System.out.println("File not found");
            } catch (NoDataException e) {
                System.out.println("File has no data");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Invalid Fields");
            }
            
            

        }

        
/* 
        String dayWithSmallestTempSpread = "Someday";     // Your day analysis function call …
        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);

        String teamWithSmallestGoalSpread = "A good team"; // Your goal analysis function call …
        System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallestGoalSpread); */
    }
    
}
