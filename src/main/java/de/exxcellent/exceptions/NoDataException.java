package de.exxcellent.exceptions;

/**
 * Custom Exception if a file exists but has no data to read.
 */
public class NoDataException extends Exception { 
    public NoDataException(String errorMessage) {
        super(errorMessage);
    }
}