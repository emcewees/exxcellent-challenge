package de.exxcellent.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;




/**
 * CSVReader to read CSV Files.
 * 
 * TODO. Adding different separator support.
 */
public class CSVReader {

    private static ArrayList<String[]> data;
    private static ArrayList<String[]> data_names;

    /**
     * Iniitialises the result fields of the reading process.
     */
    public CSVReader() {
        CSVReader.data = new ArrayList<String[]>();
        CSVReader.data_names = new ArrayList<String[]>();
    }



    private final String DEFAULT_SEPARATOR = ",";

    /**
     * Tries to read the given file and stores the read data in CSVReader.data and column names in CSVReader.data_names.
     * @param fileName
     */
    public void readFile(String fileName) {

        String line = "";
        BufferedReader reader = null;
        int counter = 0;

        try {
            reader = new BufferedReader(new FileReader(fileName));
            while ((line = reader.readLine()) != null) {
                
                // use comma as separator
                String[] fields =  line.split(DEFAULT_SEPARATOR);
                if (counter++==0) {
                    CSVReader.data_names.add(fields);
                } else {

                    CSVReader.data.add(fields);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Exposes the length of the read data.
     * @return length of read data
     */
    public static int getDataLength() {
        return CSVReader.data.size();
    }

    /**
     * Exposes the read data.
     * @return read data
     */
    public static ArrayList<String[]> getData() {
        return CSVReader.data;
    }

    /**
     * Exposes identifiers of read data.
     * @return Names of Columns
     */
    public static ArrayList<String[]> getDataNames() {
        return CSVReader.data_names;
    }

    /**
     * Prints the read data.
     */
    public static void printData() {
        for (int i=0; i<CSVReader.data.size(); i++) {
            for (String field : CSVReader.data.get(i)) {
                System.out.printf("%s\t", field);
                
            }
            System.out.printf("\n");
        }
    }

    
    






}

