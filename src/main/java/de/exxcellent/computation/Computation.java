package de.exxcellent.computation;

import java.util.ArrayList;


  
/**
 * Multipurpose Class for Calculations
 */
public class Computation {

    /**
     * Calculates the absolute difference of two values.
     * @param a First Value
     * @param b Second Value
     * @return absolute Difference between a and b
     */
    public float calculateDiff(float a, float b) {
        return Math.abs(a-b);
        
    }

    /**
     * Calculates the minimal absolute difference in a set of values using the too input values as selectors.
     * @param input Result of some input parsing
     * @param fieldOfInterest1 Column 1 in input
     * @param fieldOfInterest2 Column 2 in input
     * @return String representation of the line in input with minimal absolute Difference in input between fieldOfInterest1 and fieldOfInterest2
     */
    public String calculateMinAbsDiff(ArrayList<String[]> input, int fieldOfInterest1, int fieldOfInterest2){
        float minDiff = -1;
        String outputName = null;

        for (int i=0; i<input.size(); i++) {
            float val1 = Float.parseFloat(input.get(i)[fieldOfInterest1]); 
            float val2 = Float.parseFloat(input.get(i)[fieldOfInterest2]); 


            float tmpDiff = calculateDiff(val1, val2);

            // First Run
            if (minDiff < 0) {
                minDiff = tmpDiff;
                outputName = input.get(i)[0]; // Hard coded First Column of Row
            } else {
                if (minDiff > tmpDiff) {
                    minDiff = tmpDiff;
                    outputName = input.get(i)[0];
                }
            }
        }

        return outputName + ": " + Float.toString(minDiff);
    }
}