package de.exxcellent.challenge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Example JUnit4 test case.
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public class AppTest {

    private String successLabel = "not successful";

    @BeforeEach
    public void setUp() {
        successLabel = "successful";
    }

    @Test
    public void aPointlessTest() {
        assertEquals("successful", successLabel, "My expectations were not met");
    }

    @Test
    public void runFootball() {
        App.main("--football", "football.csv");
    }

    @Test
    public void runWeather()  {
        App.main("--weather", "weather.csv");
    }

    @Test
    public void runWeather1()  {
        App.main("--weather", "weather.csv", "-n1", "0", "-n2", "20");
    }

    @Test
    public void runWeather2()  {
        App.main("--weather");
    }

    @Test
    public void runWeather3()  {
        App.main("--weather", "weather_xy.csv");
    }

    @Test
    public void runWeather4()  {
        App.main("--weather", "weather_test1.csv");
    }

    @Test
    public void runWeather5()  {
        App.main("--weather", "weather_test2.csv");
    }

    @Test
    public void runWeather6()  {
        App.main("--weather", "weather_test3.csv");
    }

    

    @Test
    public void runOther() {
        App.main("--other");
    }

    @Test
    public void runNone() {
        App.main("");
    }

}